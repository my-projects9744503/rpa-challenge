class config:
    """
        Core information used in the "ChalengePixeldu.py" process. The version value
        can be changed (following the rules) without affecting the structure of the
        main code, but the project name should remain the same for identification
        purposes.

        Rules to Follow:
        PROJECT_NAME: String (.py format)
        VERSION: String (Versioning format i.e.: "1.0", "2.1")
    """

    PROJECT_NAME = "ChallengePixeldu.py"
    VERSION = "1.0"

class dataConfig:
    """
        Values used in the "ChalengePixeldu.py" process. Those values can be
        changed (following the rules) without affecting the structure of the
        main code.

        Rules to Follow:
        URL: String (website URL pattern)
        SEARCH_PHRASE: String
        NEWS_CATEGORY: String
        MONTHS: Integer
        SHEET_NAME: String
        EXCEL_FILE_NAME: String (.xlsx format)
    """

    URL = r'https://www.nytimes.com'
    SEARCH_PHRASE = '2024 Economy'
    NEWS_CATEGORY = 'Business'
    MONTHS = 3
    SHEET_NAME = 'news_data'
    EXCEL_FILE_NAME = 'NY_Times_Data.xlsx'

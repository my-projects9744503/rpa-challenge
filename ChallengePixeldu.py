# Import the necessary Packages for the execution of the process
import re
import os
import time
import requests
import openpyxl
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from datetime import datetime, timedelta
from Config import config as c
from Config import dataConfig as d

"""
    The code below is a solution created for the Pixeldu RPA Challenge. This process is responsible
    for accessing the New York Times website, search for a provided phrase, after that, the bot will
    be responsible for applying the appropriate filters (based on the given parameters) and sorting 
    the news list by newest first, to ease the navigation. Lastly, the bot will gather some info 
    (title, description, news picture, the count of how many times the search phrase appears in the 
    description and title and if there is any mention of money in the news title or description) and save it all
    in the appropriate Excel file.
"""


class NYTimesAutomation:
    def __init__(self):
        """
            Initializes an instance of the class and creates a Chrome WebDriver (Selenium) instance.
        """
        self.driver = webdriver.Chrome() # Initialize Chrome WebDriver (Selenium) instance
        self.driver.implicitly_wait(10) # Set implicit wait time to handle dynamic page elements to 10s

    def open_website(self):
        """
            Navigates via browser (Google Chrome) to the New York Times website.
        """
        attempts = 0 # Starts the attempts variable

        # Method to initialize the Chrome WebDriver with retry mechanism. The maximum number of attempts will be 3.
        while attempts < 3:
            try:
                self.driver.get(d.URL) # Navigates to the provided URL
                cookies_confirmation = WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located((By.XPATH, "/html/body/div[1]/div/div[2]/div/div/div/div[3]/div[2]/button[2]")))  # Obtains the "Accept all" cookies button element (timeout after 30s)
                time.sleep(3) # Wait 3s
                cookies_confirmation.click() # Click on the "Accept all" cookies button
                time.sleep(1) # Wait 1s
            except Exception as e:
                print(f"\nError acessing the website! An exception occurred: {e}\n")
                print(f"\nRetrying... (Attempt {attempts}/3\n")
                attempts += 1

    def search_news(self, search_phrase, news_category, months):
        """
            Inputs the search phrase in the search field, confirms the search pressing the "Go" button,
            and then applies filters and retrieves news based on the provided parameters.

            :param str search_phrase: Search phrase to be considered on the filter. Obtained by config file.
            :param str news_category: News Category to be considered on the filter. Obtained by config file.
            :param int months: Months to be considered on the filter. Obtained by config file.
        """

        search = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[2]/div[2]/header/section[1]/div[1]/div[2]/button") # Obtains the search field icon element
        search.click() # Click on the icon to show the search field
        search_input = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[2]/div[2]/header/section[1]/div[1]/div[2]/div/form/div/input") # Obtains the search field element
        search_input.send_keys(search_phrase) # Inputs the search phrase
        submit_button = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[2]/div[2]/header/section[1]/div[1]/div[2]/div/form/button") # Obtains the submit button element
        submit_button.click() # Click on the submit button

        initial_date, final_date = self.apply_filters(news_category, months) # Applies news category filters based on the provided parameters
        self.get_news(initial_date, final_date, search_phrase, news_category, months) # Stores all news shown in the page after applying filters

    def apply_filters(self, news_category, months):
        """
            Applies news category filters based on the provided parameters.

            :param str news_category: News Category to be considered on the filter. Obtained by config file.
            :param int months: Months to be considered on the filter. Obtained by config file.
        """
        # Date Range
        date_range_dropdown = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[2]/main/div/div[1]/div[2]/div/div/div[1]/div/div/button") # Obtains the "Date Range" dropdown element
        date_range_dropdown.click() # Click on the "Date Range" dropdown to expand options

        specific_dates = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[2]/main/div/div[1]/div[2]/div/div/div[1]/div/div/div/ul/li[6]/button") # Obtains the "Specific Dates" option element
        specific_dates.click() # Click on the "Specific Dates" option

        initial_date, final_date = self.get_range(months) # Gets the appropriate range based on the provided parameters of months

        initial_date_field = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[2]/main/div/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/div/label[1]/input") # Obtains the initial date from the range element
        final_date_field = self.driver.find_element(By.XPATH, "/html/body/div[2]/div[2]/main/div/div[1]/div[2]/div/div/div[1]/div/div/div/div[2]/div/label[2]/input") # Obtains the initial date from the range element

        initial_date_field.send_keys(initial_date) # Fill the initial date field with the correct date
        final_date_field.send_keys(final_date) # Fill the final date field with the correct date

        confirm_filter = self.driver.find_element(By.XPATH,"/html/body/div[2]/div[2]/main/div/div[1]/div[1]/form/div[1]/button") # Obtains the confirmation button
        confirm_filter.click() # Confirms applied filter

        # Section
        sections_dropdown = self.driver.find_element(By.XPATH,"/html/body/div[2]/div[2]/main/div/div[1]/div[2]/div/div/div[2]/div/div/button")  # Obtains the "Sections" dropdown element
        sections_dropdown.click()  # Click on the "Sections" dropdown to expand options
        section = 2 # Standard value

        while section <= 11: # For each available section
            if news_category in self.driver.find_element(By.XPATH,f"/html/body/div[2]/div[2]/main/div/div[1]/div[2]/div/div/div[2]/div/div/div/ul/li[{section}]/label").text: # If the current section matches the news_category parameter
                self.driver.find_element(By.XPATH,f"/html/body/div[2]/div[2]/main/div/div[1]/div[2]/div/div/div[2]/div/div/div/ul/li[{section}]/label").click() # Click on current section
                break
            else:
                section += 1 # Adds to counter

        confirm_filter.click() # Confirms applied filter

        # Sort the news by the newest options first
        sort_by_select = self.driver.find_element(By.XPATH,"/html/body/div[2]/div[2]/main/div/div[1]/div[1]/form/div[2]/div/select") # Obtains the sort by element
        sort_by_option = Select(sort_by_select) # Create a Select object based on the sort by dropdown element
        sort_by_option.select_by_value("newest") # Choose an option by value "newest"


        return initial_date, final_date


    def get_range(self, months):
        """
            Gets the appropriate range based on the provided parameters of months.

            :param int months: Months to be considered on the filter. Obtained by config file.
            :return str initial_date: Initial date to be applied on the filter.
            :return str final_date: Final date to be applied on the filter.
        """
        today = datetime.now() # Today's date

        # Calculates the first day of the current month - parameter "months"
        first_day_last_month = today.replace(day=1) - timedelta(days=today.day)
        initial_date = (first_day_last_month - timedelta(days=30 * (months - 1))).strftime("%m/%Y")
        date_position = initial_date.find("/")
        initial_date = initial_date[:date_position] + "/01" + initial_date[date_position:]

        # Calculates the last day of the current month
        first_day_next_month = datetime(today.year, today.month + 1, 1)
        final_date = first_day_next_month - timedelta(days=1)
        final_date = final_date.strftime("%m/%d/%Y")

        return initial_date, final_date

    def get_news(self, initial_date_string, final_date_string, search_phrase, news_category, months):
        """
            Retrieves the news based on the applied filters, extracting the information from the relevant ones.

            :param str initial_date_string: Initial date to be applied on the filter. Obtained based on the "months" parameter.
            :param str final_date_string: Final date to be applied on the filter. Obtained based on the current month.
            :param str search_phrase: Search phrase to be considered on the filter. Obtained by config file.
            :param str news_category: News Category to be considered on the filter. Obtained by config file.
            :param int months: Months to be considered on the filter. Obtained by config file.
        """

        finished_process = False # Defines standard value to boolean
        news_count = 1 # Defines initial value to counter

        while finished_process is False:
            # Time and date variable for Log purposes
            current_time = datetime.now()
            current_date = current_time.strftime("%d/%m/%Y")
            current_time_in_hours = current_time.strftime("%H:%M:%S")

            if news_count > 11 and (news_count - 1) % 11 == 0: # Verifies the need to click on the "Show More" button, which must be clicked every 11 news
                self.driver.find_element(By.XPATH, "/html/body/div[2]/div[2]/main/div/div[2]/div[2]/div/button").click() # Click on the "Show More" button

            try:
                # Try to obtain the publication date of the current news
                date_text = self.driver.find_element(By.XPATH, f'/html/body/div[2]/div[2]/main/div/div[2]/div[1]/ol/li[{news_count}]/div/span').text # Obtains the current news date element
            except:
                # In some cases, there are blank spaces between news that need to be ignored

                # Register Log
                print("\n# CURRENT NEWS UPDATE #\n")
                print("> Time:", current_time_in_hours)
                print("> Current News Counter:", news_count)
                print("> Status:", "Blank space detected!")
                print("---------------------------------")

                news_count += 1 # Adds to counter
                continue

            if "h ago" in date_text:
                # If the publication date was hours ago, assume current day
                news_date_string = datetime.now().strftime("%m/%d/%Y") # Today's date
                news_date = datetime.strptime(news_date_string, "%m/%d/%Y") # Today's date
            else:
                # Define a mapping from month abbreviations to month numbers
                month_mapping = {
                    'Jan': '01', 'Feb': '02', 'March': '03', 'April': '04', 'May': '05', 'June': '06',
                    'July': '07', 'Aug': '08', 'Sept': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'
                }

                # Split the date string into components
                date_components = date_text.replace(',', '').split('.')

                # If the year is not present, assume the current year
                if len(date_components) == 2:
                    date_components.append(str(datetime.now().year))

                # Extract month, day, and year
                month, day, year = date_components

                year_match = re.search(r'\b\d{4}\b', day)

                if year_match:
                    year = year_match.group()
                    day = str(day).replace(year, "").strip()

                # Convert the month abbreviation to a number
                month_number = month_mapping.get(month)

                # Construct the new date string in the format "%m/%d/%Y"
                news_date_string = f"{month_number}/{day}/{year}"
                news_date_string = news_date_string.replace(" ","")

                if "None" in news_date_string:
                    # If the month is "None", the "month_mapping" variable needs to be remapped

                    # Register Log
                    print("\n# CURRENT NEWS UPDATE #\n")
                    print("> Time:", current_time_in_hours)
                    print("> Current News Counter:", news_count)
                    print("> Status:", "Need to remap the month!")
                    print("---------------------------------")
                    news_count += 1 # Adds to counter
                    continue

                news_date = datetime.strptime(news_date_string, "%m/%d/%Y") # Converts the date to datetime

            initial_date = datetime.strptime(initial_date_string, "%m/%d/%Y") # Converts the date to datetime
            final_date = datetime.strptime(final_date_string, "%m/%d/%Y") # Converts the date to datetime

            if final_date >= news_date >= initial_date: # If the date is within the stipulated range
                self.process_news(news_date, search_phrase, news_count) # Extracts and save the information from current news

                # Register Log
                print("\n# CURRENT NEWS UPDATE #\n")
                print("> Time:", current_time_in_hours)
                print("> Current News Counter:", news_count)
                print("> Status:", "Successfully registered!")
                print("---------------------------------")

                news_count += 1 # Adds to counter
            else:
                # Since the news are sorted by newest first, if an option outside the range was found, it means there are no other news to extract

                # Register Log
                print("\n# END OF PROCESS #\n")
                print("> Time:", current_time_in_hours)
                print("> News Analyzed:", news_count)
                print("> Status:", "Process finished!")
                print("---------------------------------")

                finished_process = True # Defines boolean to True, meaning the process is over
                break

    def process_news(self, news_date, search_phrase,news_count):
        """
            Extracts the information from each news shown in the page, searching for the title, description,
            image URL, and then saves all the information collect calling another function.

            :param str news_date: Date of publication of the current news. Obtained through extraction from the website.
            :param str search_phrase: Search phrase to be considered on the filter. Obtained by config file.
            :param str news_count: Navigated news counter.
        """
        title = self.driver.find_element(By.XPATH, f"/html/body/div[2]/div[2]/main/div/div[2]/div[1]/ol/li[{news_count}]/div/div/div/a/h4").text # Obtains the title of the current news
        description_element = self.driver.find_element(By.XPATH, f"/html/body/div[2]/div[2]/main/div/div[2]/div[1]/ol/li[{news_count}]/div/div/div/a/p[1]") # Obtains the description element
        description = description_element.text if description_element.is_displayed() else "" # If not empty, obtains the description of the current news

        try:
            # Try to find the picture attached in the current news
            picture_url = self.driver.find_element(By.XPATH, f"/html/body/div[2]/div[2]/main/div/div[2]/div[1]/ol/li[{news_count}]/div/div/figure/div/img").get_attribute("src") # Obtains the image URL of the current news
            image_path = self.save_picture(picture_url, news_count) # Saves the picture path into the variable
        except:
            # In some cases, there are no picture attached
            image_path = "No picture attached!" # Update the variable to insert it into the Excel file

        count_in_title = title.lower().count(search_phrase.lower()) # Count occurrences of the search phrase in the title
        count_in_description = description.lower().count(search_phrase.lower()) # Count occurrences of the search phrase in the description

        contains_money = self.check_money_format(title, description) # Checks whether the title or description has any amount of money


        self.append_to_excel(news_count ,title, news_date.strftime("%Y-%m-%d"), description, image_path, count_in_title,count_in_description, contains_money) # Insert the information extracted into the Excel file

    def check_money_format(self, title, description):
        """
            Checks whether the title or description has any amount of money using regex (Regular Expression).

            :param str title: Title of the current news. Obtained through extraction from the website.
            :param str description: Description of the current news. Obtained through extraction from the website.
            :return bool contains_money: Returns if was found any mention of money on the Title or Description.
        """
        money_pattern = re.compile(r'\b(?:\$\s?|\d+\s?(?:dollars?|USD))[\d,.]+\b') # Using regex, defines the patterns to look for in the strings (title and description)
        return bool(re.search(money_pattern, title)) or bool(re.search(money_pattern, description)) # Search for the pattern in the title and description looking for any kind of amount of money

    def save_picture(self, picture_url, news_count):
        """
            Downloads the news image and saves it to a specified folder/path.

            :param str picture_url: Picture URL of the current news. Obtained through extraction from the website.
            :param str news_count: Navigated news counter.
            :return str image_path: Path of the saved news picture (if there is any).
        """

        self.driver.execute_script("window.open('');") # Open a new tab
        self.driver.switch_to.window(self.driver.window_handles[1]) # Switch to the new tab

        self.driver.get(picture_url) # Access the image URL
        image_content = requests.get(picture_url).content # Gets the image from the URL content


        self.driver.close() # Close the current tab
        self.driver.switch_to.window(self.driver.window_handles[0]) # Switch back to the original tab

        image_path = os.path.join(os.path.dirname(__file__), "news_images", f"news_image_{news_count}.png") # Creates the image path

        # Verify if the folder exists and, if necessary, creates one
        if not os.path.exists(os.path.join(os.path.dirname(__file__), "news_images")):
            os.makedirs(os.path.join(os.path.dirname(__file__), "news_images"))

        # Saves image in the validated path
        with open(image_path, "wb") as f:
            f.write(image_content)

        return image_path

    def append_to_excel(self, news_count , title, date, description, image_path, count_in_title, count_in_description, contains_money):
        """
            Inserts the extracted information into the "NY_Time_Data.xlsx" Excel file, always
            filling the first blank line, inserting each information into its appropriate column.

            :param str news_count: Navigated news counter.
            :param str title: Title of the current news. Obtained through extraction from the website.
            :param str description: Description of the current news. Obtained through extraction from the website.
            :param str image_path: Path of the saved news picture (if there is any).
            :param str count_in_title: The number of times the search phrase appeared in the title.
            :param str count_in_description: The number of times the search phrase appeared in the title.
            :param str contains_money:  Returns if was found any mention of money on the Title or Description.
        """

        try:
            # Try to open the existing workbook
            workbook = openpyxl.load_workbook(d.EXCEL_FILE_NAME)
            sheet = workbook[d.SHEET_NAME]
        except FileNotFoundError:
            # If the file doesn't exist, create a new workbook
            workbook = Workbook()
            sheet = workbook.active
            sheet.title = d.SHEET_NAME

            # Add headers to the sheet
            headers = ["ID","Title", "Date", "Description", "Image Path", "Count in Title", "Count in Description",
                       "Contains Money?"]
            for col_num, header in enumerate(headers, 1):
                col_letter = get_column_letter(col_num)
                sheet[f"{col_letter}1"] = header

        # Find the first empty row in the sheet
        row_num = sheet.max_row + 1

        # Write data to the corresponding columns
        data = [news_count ,title, date, description, image_path, count_in_title, count_in_description, contains_money]
        for col_num, value in enumerate(data, 1):
            col_letter = get_column_letter(col_num)
            sheet[f"{col_letter}{row_num}"] = value

        # Save the workbook
        workbook.save(d.EXCEL_FILE_NAME)

    def close_browser(self):
        """
            Close the Chrome WebDriver instance when done with process
        """
        self.driver.quit() # Close the Chrome WebDriver instance when done


if __name__ == "__main__":
    """
        Performs the Main process, accessing the NY Times website and extracting
        all the information necessary.
    """

    # Time and date variable for Log purposes
    current_time = datetime.now()
    current_date = current_time.strftime("%d/%m/%Y")
    current_time_in_hours = current_time.strftime("%H:%M:%S")

    # Register Log
    print("\n# START OF PROCESS #\n")
    print("> Process:", c.PROJECT_NAME)
    print("> Version:", c.VERSION)
    print("> Time:", current_time_in_hours)
    print("---------------------------------")

    # Main Process
    nytimes_bot = NYTimesAutomation()
    nytimes_bot.open_website()
    nytimes_bot.search_news(d.SEARCH_PHRASE, d.NEWS_CATEGORY, d.MONTHS)
    nytimes_bot.close_browser()
